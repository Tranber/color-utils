import typescript from 'rollup-plugin-typescript';

export default {
  input: './src/index.ts',
  plugins: [
    typescript({
      typescript: require('./node_modules/typescript/'),
    })
  ],
  output: [
    {
      file: './build/lib/color-utils.js',
      format: 'cjs',
    }
  ],
};
