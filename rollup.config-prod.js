import typescript from 'rollup-plugin-typescript';
import { terser } from 'rollup-plugin-terser';
// import { uglify } from 'rollup-plugin-uglify';

export default {
  input: './src/index.ts',
  plugins: [
    typescript({
      typescript: require('./node_modules/typescript/')
    }),
    terser()
    // uglify()
  ],
  output: [
    {
      file: './build/lib/color-utils.min.js',
      format: 'cjs'
    },
    {
      file: './build/module/color-utils.mjs',
      format: 'es'
    }
  ]
};
