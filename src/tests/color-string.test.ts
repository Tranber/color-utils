import {
  ColorPattern,
  ValuePattern,
} from '../core';

test('rgb level 4 avec alpha', () => {
  expect('rgb(5 45 86 / 0.2)').toMatch(ColorPattern.RGB);
});

test('rgb level 4', () => {
  expect('rgb(5 45 86)').toMatch(ColorPattern.RGB);
});

test('rgb hex 3 sans alpha', () => {
  expect('#ff5').toMatch(ColorPattern.HEXADECIMAL);
});

test('rgb hex 3 avec alpha', () => {
  expect('#ff5e').toMatch(ColorPattern.HEXADECIMAL);
});

test('rgb level 3', () => {
  expect('rgb(5, 45, 86)').toMatch(ColorPattern.RGB);
});

test('angle degré', () => {
  expect('120deg').toMatch(new RegExp(ValuePattern.ANGLE));
});
