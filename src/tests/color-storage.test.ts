import {
  ColorStorage,
} from '../core';

const cSt = ColorStorage.validateRGB(0.2, 0.5, 1, 0.5);

test('ColorStorage constructor', () => {
  expect(cSt).toEqual([0.2, 0.5, 1, 0.5]);
});
