import {
  RGBColor,
  CSSColor,
} from '../core';

const cl1 = new RGBColor(0.2, .5, 1, 0.5);
const cl2 = new RGBColor(1, 1, 1, 1);

test('Color to hexadecimal', () => {
  expect(CSSColor.toHexadecimalString(cl2)).toEqual('#ffffff');
});

test('Color to css rgb', () => {
  expect(CSSColor.toString(cl1)).toEqual('rgb(51 127.5 255)');
});
