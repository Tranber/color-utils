class Matrix {
  private _matrix: number[];
  private _cols: number;
  private _rows: number;

  constructor(values: number[][]) {
    this._rows = values.length;
    this._cols = values[0].length;
    this._matrix = []; // new Array(this._rows * this._cols);

    values.forEach(value => {
      value.forEach(item => {
        this._matrix.push(item); // index courant ???
      });
    });
  }
}

export default Matrix;
