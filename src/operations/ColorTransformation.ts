import { RGBColor, NamedColor, ColorModelConversion } from '../core';
import { NumberValue, Color } from '../types';

function mixChannels(channel1: number, channel2: number, percent) {
  return channel1 * (100 - percent) + channel2 * percent;
}

class ColorTransformation {
  static blend(color1: Color, color2: Color) {
    let rgbColor1: RGBColor, rgbColor2: RGBColor;
    if (!(color1 instanceof RGBColor)) {
      rgbColor1 = ColorModelConversion.toRGBColor(color1);
    }
    if (!(color2 instanceof RGBColor)) {
      rgbColor2 = ColorModelConversion.toRGBColor(color2);
    }
    // TODO calcul
  }

  static mix(color1: Color, color2: Color, colorPercent: NumberValue) {
    let rgbColor1: RGBColor, rgbColor2: RGBColor;
    if (!(color1 instanceof RGBColor)) {
      rgbColor1 = ColorModelConversion.toRGBColor(color1);
    }
    if (!(color2 instanceof RGBColor)) {
      rgbColor2 = ColorModelConversion.toRGBColor(color2);
    }

    const r = mixChannels(rgbColor1.r, rgbColor2.r, colorPercent);
    const g = mixChannels(rgbColor1.g, rgbColor2.g, colorPercent);
    const b = mixChannels(rgbColor1.b, rgbColor2.b, colorPercent);
    const a = mixChannels(rgbColor1.a, rgbColor2.a, colorPercent);

    return new RGBColor(r, g, b, a);
  }

  static tint(color: Color, colorPercent: NumberValue) {
    return this.mix(NamedColor.white, color, colorPercent);
  }

  static shade(color: Color, colorPercent: NumberValue) {
    return this.mix(NamedColor.black, color, colorPercent);
  }
}

export default ColorTransformation;
