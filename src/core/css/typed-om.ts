class CSSStyleValue {}
class CSSNumericValue extends CSSStyleValue {
  // static parse(cssText: string): CSSNumericValue {}
}

class CSSUnitValue extends CSSNumericValue {
  constructor(public value: number, readonly unit: string) {
    super();
  }
}

export { CSSStyleValue, CSSNumericValue, CSSUnitValue };
