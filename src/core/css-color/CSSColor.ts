import { RGBColor, HSLColor, HWBColor, ColorModelConversion } from '../color';
import ColorStorage from '../ColorStorage';
import { ValueType } from './constants';
import {
  parse,
  formatHueValue,
  formatAlphaValue,
  formatColorValue
} from './utils';
import { Color } from '../../types';

class CSSColor {
  static fromString(cssString: string): Color {
    return parse(cssString);
  }

  static toHexadecimalString(color: Color, withAlpha = false): string {
    color = ColorModelConversion.toRGBColor(color);

    const r = (color.r * 255).toString(16);
    const g = (color.g * 255).toString(16);
    const b = (color.b * 255).toString(16);
    const a = (color.a * 255).toString(16);

    if (withAlpha) {
      return `#${r}${g}${b}${a}`;
    }

    return `#${r}${g}${b}`;
  }

  static toString(
    color: Color,
    config = {
      alpha: undefined,
      color: ValueType.INT_BYTE,
      hue: ValueType.FLOAT_ANGLE,
      legacy: false
    }
  ): string {
    const {
      alpha: alphaFormat,
      color: colorFormat,
      hue: hueFormat,
      legacy
    } = config;
    const alphaStr = alphaFormat === undefined ? '' : 'a';
    let alphaValue =
      alphaFormat === undefined
        ? ''
        : ` / ${formatAlphaValue(color.a, alphaFormat)}`;
    if (legacy) {
      alphaValue =
        alphaFormat === undefined
          ? ''
          : `, ${formatAlphaValue(color.a, alphaFormat)}`;
    }

    if (color instanceof HSLColor) {
      const hueValue = ColorStorage.validateStorageValue(color.h);
      const saturationValue = ColorStorage.validateStorageValue(color.s);
      const lightnessValue = ColorStorage.validateStorageValue(color.l);

      if (legacy) {
        return `hsl${alphaStr}(${formatHueValue(
          hueValue,
          hueFormat
        )}, ${saturationValue * 100}%, ${lightnessValue * 100}%${alphaValue})`;
      }

      return `hsl(${formatHueValue(hueValue, hueFormat)} ${saturationValue *
        100}% ${lightnessValue * 100}%${alphaValue})`;
    }

    if (color instanceof HWBColor) {
      const hueValue = ColorStorage.validateStorageValue(color.h);
      const whitenessValue = ColorStorage.validateStorageValue(color.w);
      const blacknessValue = ColorStorage.validateStorageValue(color.b);

      return `hwb(${formatHueValue(hueValue, hueFormat)} ${whitenessValue *
        100}% ${blacknessValue * 100}%${alphaValue})`;
    }

    if (color instanceof RGBColor) {
      const redValue = ColorStorage.validateStorageValue(color.r);
      const greenValue = ColorStorage.validateStorageValue(color.g);
      const blueValue = ColorStorage.validateStorageValue(color.b);

      if (legacy) {
        return `rgb${alphaStr}(${formatColorValue(
          redValue,
          colorFormat
        )}, ${formatColorValue(greenValue, colorFormat)}, ${formatColorValue(
          blueValue,
          colorFormat
        )}${alphaValue})`;
      }

      return `rgb(${formatColorValue(redValue, colorFormat)} ${formatColorValue(
        greenValue,
        colorFormat
      )} ${formatColorValue(blueValue, colorFormat)}${alphaValue})`;
    }
  }
}

export default CSSColor;
