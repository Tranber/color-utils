import ColorStorage from '../ColorStorage';

class HWBColor {
  private _color: number[];

  constructor(h = 0, w = 0, b = 0, a = 1) {
    this._color = ColorStorage.validateHWB(h, w, b, a);
  }

  get hwba() {
    return [...this._color];
  }

  get hwb() {
    return [this._color[0], this._color[1], this._color[2]];
  }

  get h() {
    return this._color[0];
  }

  set h(hV) {
    this._color[0] = ColorStorage.validateStorageValue(hV);
  }

  get w() {
    return this._color[1];
  }

  set w(wV) {
    this._color[1] = ColorStorage.validateStorageValue(wV);
  }

  get b() {
    return this._color[2];
  }

  set b(bV) {
    this._color[2] = ColorStorage.validateStorageValue(bV);
  }

  get a() {
    return this._color[3];
  }

  set a(aV) {
    this._color[3] = ColorStorage.validateStorageValue(aV);
  }
}

export default HWBColor;
