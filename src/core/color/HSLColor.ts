import ColorStorage from '../ColorStorage';

class HSLColor {
  private _color: number[];

  constructor(h = 0, s = 0, l = 0, a = 1) {
    this._color = ColorStorage.validateHSL(h, s, l, a);
  }

  get hsla() {
    return [...this._color];
  }

  get hsl() {
    return [this._color[0], this._color[1], this._color[2]];
  }

  get h() {
    return this._color[0];
  }

  set h(hV) {
    this._color[0] = ColorStorage.validateStorageValue(hV);
  }

  get s() {
    return this._color[1];
  }

  set s(sV) {
    this._color[1] = ColorStorage.validateStorageValue(sV);
  }

  get l() {
    return this._color[2];
  }

  set l(lV) {
    this._color[2] = ColorStorage.validateStorageValue(lV);
  }

  get a() {
    return this._color[3];
  }

  set a(aV) {
    this._color[3] = ColorStorage.validateStorageValue(aV);
  }
}

export default HSLColor;
