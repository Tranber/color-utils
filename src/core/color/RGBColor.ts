import ColorStorage from '../ColorStorage';
import { ColorModel } from './constants';
import Color from './Color';

class RGBColor extends Color {
  private _color: number[];

  constructor(r = 0, g = 0, b = 0, a = 1) {
    super(ColorModel.RGB);
    this._color = ColorStorage.validateRGB(r, g, b, a);
  }

  get rgba() {
    return [...this._color];
  }

  get rgb() {
    return [this._color[0], this._color[1], this._color[2]];
  }

  get color() {
    return this._color;
  }

  get r() {
    return this._color[0];
  }

  set r(rV) {
    this._color[0] = ColorStorage.validateStorageValue(rV);
  }

  get g() {
    return this._color[1];
  }

  set g(gV) {
    this._color[1] = ColorStorage.validateStorageValue(gV);
  }

  get b() {
    return this._color[2];
  }

  set b(bV) {
    this._color[2] = ColorStorage.validateStorageValue(bV);
  }

  get a() {
    return this._color[3];
  }

  set a(aV) {
    this._color[3] = ColorStorage.validateStorageValue(aV);
  }
}

export default RGBColor;
