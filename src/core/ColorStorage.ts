import { NumberValue } from '../types';

function clampStorageValue(value: number, config = { min: 0, max: 1 }): number {
  const { min, max } = config;

  if (value < min) {
    return min;
  }
  if (value > max) {
    return max;
  }

  return value;
}

class ColorStorage {
  static validateStorageValue(value: NumberValue): number {
    if (typeof value === 'number') {
      return clampStorageValue(value);
    }
    if (typeof value === 'string') {
      value = parseFloat(value);

      if (Number.isNaN(value)) {
        throw new TypeError('Value can not be converted to a number');
      }

      return clampStorageValue(value);
    }

    throw new TypeError('Value should be a number');
  }

  static validateRGB(r: number, g: number, b: number, a: number) {
    return [
      this.validateStorageValue(r), // r
      this.validateStorageValue(g), // g
      this.validateStorageValue(b), // b
      this.validateStorageValue(a) // a
    ];
  }

  static validateHSL(h: number, s: number, l: number, a: number) {
    return [
      this.validateStorageValue(h), // h
      this.validateStorageValue(s), // s
      this.validateStorageValue(l), // l
      this.validateStorageValue(a) // a
    ];
  }

  static validateHWB(h: number, w: number, b: number, a: number) {
    return [
      this.validateStorageValue(h), // h
      this.validateStorageValue(w), // w
      this.validateStorageValue(b), // b
      this.validateStorageValue(a) // a
    ];
  }
}

export default ColorStorage;
